package peterlavalle

trait PiPairT {

	implicit class PiPair[L, R](pair: (L, R)) {
		def mapr[O](f: R => O): (L, O) =
			(pair._1, f(pair._2))

		def forl(f: L => Unit): R = {
			val (l, r) = pair
			f(l)
			r
		}
	}

}
