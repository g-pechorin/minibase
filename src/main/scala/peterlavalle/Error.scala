package peterlavalle

import java.io._
import java.util
import java.util.Date
import scala.reflect.ClassTag

object Error {
	private val toDone = new util.HashSet[String]()
}

trait Error {

	def TODO(message: String): Unit = {

		val frame: String =
			Thread.currentThread()
				.getStackTrace
				.toList
				.tail
				.filterNot((_: StackTraceElement).getMethodName.matches("TODO\\$?")).head.toString()

		val text: String =
			if (null == message)
				frame
			else
				frame + "\n\t" + message

		Error.toDone.synchronized {
			if (Error.toDone.add(text))
				System.err.println(text)
		}
	}

	def ??? : Nothing = {
		val error = new NotImplementedError()
		error.setStackTrace {
			error.getStackTrace
				.dropWhile((_: StackTraceElement).getMethodName.matches("\\$qmark\\$qmark\\$qmark\\$?"))
		}
		throw error
	}

	def require(requirement: Boolean, message: Any = "failed"): Unit = if (!requirement) {
		val error = new RuntimeException(message.toString)

		error.setStackTrace(
			error.getStackTrace
				.dropWhile {
					frame: StackTraceElement =>
						"require" == frame.getMethodName || "require$" == frame.getMethodName
				}
				.toList
				.filterNext {
					(l: StackTraceElement, r: StackTraceElement) =>
						l.getFileName != r.getFileName || l.getLineNumber != r.getLineNumber
				}.toArray
		)

		throw error
	}

	def raise(throwable: Throwable): Nothing = {
		System.out.flush()
		System.err.flush()
		throwable.printStackTrace(System.err)
		System.err.flush()
		throw throwable
	}

	def expect(requirement: Boolean, message: Any = "unexpected"): Boolean = {
		if (!requirement) {
			val error = new RuntimeException(message.toString)

			error.setStackTrace(
				error.getStackTrace
					.dropWhile {
						frame: StackTraceElement =>
							"expect" == frame.getMethodName || "expect$" == frame.getMethodName
					}
					.toList
					.filterNext {
						(l: StackTraceElement, r: StackTraceElement) =>
							l.getFileName != r.getFileName || l.getLineNumber != r.getLineNumber
					}
					.toArray
					.take(8)
			)

			System.out.flush()
			System.err.flush()
			error.printStackTrace(System.err)
			System.err.flush()
		}
		requirement
	}

	def error(message: Any): Nothing = {
		val error = new RuntimeException(message.toString)

		error.setStackTrace(
			error.getStackTrace
				.dropWhile {
					frame: StackTraceElement =>
						"error" == frame.getMethodName || "error$" == frame.getMethodName
				}
				.toList
				.filterNext {
					(l: StackTraceElement, r: StackTraceElement) =>
						l.getFileName != r.getFileName || l.getLineNumber != r.getLineNumber
				}.toArray
		)
		error.printStackTrace(System.err)

		throw error
	}

	sealed trait intercept {
		def in[O](action: => O): O

		def swallow(action: => Unit): Unit
	}

	object intercept {
		val used = Set(
			"awt.toolkit",
			"file.encoding",
			"file.separator",
			"java.awt.graphicsenv",
			"java.awt.printerjob",
			"java.class.version",
			"java.runtime.name",
			"java.runtime.version",
			"java.specification.name",
			"java.specification.vendor",
			"java.specification.version",
			"java.vendor",
			"java.vendor.url",
			"java.vendor.url.bug",
			"java.vendor.version",
			"java.version",
			"java.version.date",
			"java.vm.compressedOopsMode",
			"java.vm.info",
			"java.vm.name",
			"java.vm.specification.name",
			"java.vm.specification.vendor",
			"java.vm.specification.version",
			"java.vm.vendor",
			"java.vm.version",
			"jdk.debug",
			"jline.esc.timeout",
			"jline.shutdownhook",
			"jna.loaded",
			"jna.nosys",
			"log4j.defaultInitOverride",
			"log4j.ignoreTCL",
			"os.arch",
			"os.name",
			"os.version",
			"path.separator",
			"sun.arch.data.model",
			"sun.awt.enableExtraMouseButtons",
			"sun.cpu.endian",
			"sun.cpu.isalist",
			"sun.desktop",
			"sun.io.unicode.encoding",
			"sun.java.launcher",
			"sun.jnu.encoding",
			"sun.management.compiler",
			"sun.os.patch.level",
			"sun.stderr.encoding",
			"sun.stdout.encoding",
			"user.country",
			"user.language",
			"user.script",
			"user.timezone",
			"user.variant",
		)

		/**
		 * used to catch/intercept (runtime?) exceptions and write them to a file for later analysis
		 */
		def apply[E <: Exception : ClassTag](file: String): intercept =
			new intercept {

				def dump(x: Throwable): Unit = {

					val logFile: File = new File(file).EnsureParent

					System.err.println("log ; " + logFile.AbsolutePath)
					System.err.flush()

					val writer: Writer =
						System.getProperties
							.keySet()
							.map((_: AnyRef).toString)
							.toList
							.filter(used.contains)
							.sorted
							.foldLeft(
								new FileWriter(logFile, true)
									.append(new Date().iso8061Long + "\n")
							) {
								case (out, key) =>
									out.append(key + ":" + System.getProperty(key) + "\n")
							}

					def loop(e: Throwable): Unit =
						if (null == e)
							writer.close()
						else {

							val stringWriter = new StringWriter()
							e.printStackTrace(new PrintWriter(stringWriter))

							writer.append(e.getClass + "; " + e.getMessage + "\n")
							stringWriter.toString
								.split("[\r \t]*\n")
								.foreach(writer.append("\t> ").append(_).append("\n"))

							loop(e.getCause)
						}

					loop(x)
				}

				override def in[O](action: => O): O =
					try {
						action
					} catch {
						case x if implicitly[ClassTag[E]].runtimeClass.isInstance(x) =>
							dump(x)
							throw x
					}

				override def swallow(action: => Unit): Unit =
					try {
						action
					} catch {
						case x if implicitly[ClassTag[E]].runtimeClass.isInstance(x) =>
							dump(x)
					}
			}
	}
}
