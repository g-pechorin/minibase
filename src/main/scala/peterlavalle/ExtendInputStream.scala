package peterlavalle

import java.io.InputStream
import scala.annotation.tailrec

trait ExtendInputStream {

	implicit class extendInputStream(stream: InputStream) {
		def readBytes(): Array[Byte] = {
			val chunk = readBytes(32)
			if (chunk.nonEmpty)
				chunk ++ readBytes()
			else
				chunk
		}

		def readBytes(len: Int): Array[Byte] = {
			val into: Array[Byte] = Array.ofDim[Byte](len)

			@tailrec
			def loop(done: Int): Array[Byte] =
				stream.read(into, done, len - done) match {
					case -1 | 0 =>
						into.take(done)

					case read =>
						loop(done + read)
				}

			loop(0)
		}
	}

}
