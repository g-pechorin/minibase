package peterlavalle.io

import peterlavalle._

import java.io.{ByteArrayInputStream, File, FileInputStream, InputStream}
import java.util.zip.ZipInputStream
import scala.io.{BufferedSource, Source}

trait ReadSource extends ReadSource.T {

	/**
	 * list of all files in the stuff with full paths
	 */
	def list: Set[String]

	def open(name: String): InputStream
}

object ReadSource {

	val Fresh: ReadSource = {
		object Fresh extends ReadSource {
			override val list: Set[String] = Set()

			override def open(name: String): InputStream = null
		}

		Fresh
	}

	def apply(file: File): ReadSource = {
		require(file.exists(), "can't find file " + file.AbsolutePath)
		if (file.isDirectory)
			fromDir(file.getAbsoluteFile)
		else
			fromJar(file.getAbsoluteFile)
	}

	def fromDir(file: File): ReadSource =
		new ReadSource {
			override lazy val list: Set[String] =
				file.**().toSet

			override def open(name: String): InputStream = new FileInputStream(file / name)
		}

	def fromJar(file: File): ReadSource =
		new ReadSource {
			override lazy val list: Set[String] = {

				new ZipInputStream(new FileInputStream(file))
					.using {
						stream =>
							Stream
								.continually(stream.getNextEntry)
								.takeWhile(null != _)
								.filterNot(_.isDirectory)
								.map(_.getName)
								.toSet
					}
			}

			override def open(name: String): InputStream =
				error("open an zip entry into an input stream")
		}

	sealed trait T {
		this: ReadSource =>

		/**
		 * exclusive concat. append all entried in the "next" one to this, but, don't allow duplicated names
		 *
		 * @param next the next instance to combine into us
		 * @return an instances that combines us
		 */
		final def combine(next: ReadSource): ReadSource = {
			val self: ReadSource = this

			// require unique
			require(!self.list.exists(next.list))

			// just combine it
			self cascade next
		}

		/**
		 * combine of these using the entry from us in the case of a duplicate
		 */
		final def cascade(next: ReadSource): ReadSource = {
			val self: ReadSource = this
			new ReadSource {
				override def list: Set[String] = self.list ++ next.list

				override def open(name: String): InputStream =
					if (self.list contains name)
						self.open(name)
					else if (next.list contains name)
						next.open(name)
					else
						null
			}
		}

		/**
		 * turn this/us into a big stream of key:stream
		 */
		final def read: Stream[(String, InputStream)] =
			list
				.toStream
				.map {
					name: String =>
						name -> open(name)
				}

		/**
		 * create a new instance that only includes sub files and refers to them by their "sub name"
		 */
		final def subsystem(prefix: String): ReadSource = {
			require(prefix.endsWith("/"))
			exclusively((_: String) startsWith prefix)
				.rename((_: String) drop prefix.length)
		}

		final def rename(f: String => String): ReadSource = {
			val self: ReadSource = this

			val pairs: List[(String, String)] =
				self.list.toList.map((l: String) => l -> f(l))

			new ReadSource {
				override def list: Set[String] = pairs.map((_: (String, String))._2).toSet

				override def open(name: String): InputStream =
					self.open {
						pairs.find((_: (String, String))._2 == name).get._1
					}
			}
		}

		/**
		 * rewrite files as strings
		 *
		 * @param p the predicate to control which to rewrite
		 * @param f the rwriting function
		 * @return an instance with rewritten names
		 */
		final def rewrite(p: String => Boolean)(f: (String, String) => String): ReadSource =
			replace {
				i: String =>
					if (!p(i))
						None
					else
						Some {
							src: InputStream =>
								Source.fromInputStream(src)
									.using {
										src: BufferedSource =>
											new ByteArrayInputStream(
												f(i, src.mkString)
													.getBytes("utf-8")
											)
									}
						}
			}

		final def replace(f: String => Option[InputStream => InputStream]): ReadSource = {

			val self: ReadSource = this

			new ReadSource {
				override lazy val list = self.list

				override def open(name: String): InputStream = {
					val from: InputStream = self.open(name)

					f(name)
						.map((_: InputStream => InputStream) apply from)
						.getOrElse(from)
				}
			}
		}

		/**
		 * produce an instance that ignores/drops files
		 *
		 * @param p predicate to control what's ignored
		 * @return
		 */
		final def ignore(p: String => Boolean): ReadSource = exclusively((n: String) => !p(n))

		/**
		 * produce an instance that only includes entries which pass this check
		 */
		final def exclusively(must: String => Boolean): ReadSource = {
			val self: ReadSource = this
			new ReadSource {
				override lazy val list: Set[String] = self.list.filter(must)

				override def open(name: String): InputStream =
					if (!must(name))
						null
					else
						self.open(name)
			}
		}
	}
}
