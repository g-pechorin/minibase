package peterlavalle

trait Maybe[+V] {
	def map[O](f: V => O): Maybe[O]

	//	def flatMap[O](f: V => Option[O]): Maybe[O]

	def flatMap[O](f: V => Maybe[O]): Maybe[O]

	def get: V

	def isEmpty: Boolean

	@inline def nonEmpty: Boolean = !isEmpty

	@inline final def orElse[B >: V](alternative: => Maybe[B]): Maybe[B] =
		if (isEmpty) alternative else this

	def toOption: Option[V] =
		if (isEmpty)
			None
		else
			Some(get)
}

object Maybe {

	lazy val None = {
		@inline
		case object None extends Maybe[Nothing] {
			@inline def isEmpty = true

			@inline def get = throw new NoSuchElementException("None.get")

			@inline override def map[O](f: Nothing => O): Maybe[O] = this

			//			@inline override def flatMap[O](f: Nothing => Option[O]): Maybe[O] = this

			@inline override def flatMap[O](f: Nothing => Maybe[O]): Maybe[O] = this
		}
		None
	}

	def unapply[V](arg: Maybe[V]): Option[V] = arg.toOption

	def fromOption[V](v: => Option[V]): Maybe[V] =
		Maybe(v).flatMap {
			(_: Option[V]) match {
				case Some(value) => Maybe(value)
				case scala.None => Maybe.None
			}
		}

	def apply[V](v: => V): Maybe[V] =
		new Maybe[V] {
			@inline override def map[O](f: V => O): Maybe[O] = Maybe(f(get))

			//			override def flatMap[O](f: V => Option[O]): Maybe[O] = fromOption(f(get))

			override def flatMap[O](f: V => Maybe[O]): Maybe[O] = f(get)

			@inline override lazy val get: V = v

			@inline override def isEmpty: Boolean = false
		}
}
