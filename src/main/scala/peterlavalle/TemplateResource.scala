package peterlavalle

import java.io.InputStream
import java.util
import scala.io.{BufferedSource, Source}
import scala.reflect.ClassTag
import scala.util.matching.Regex

trait TemplateResource {
	lazy val rSlot: Regex = {
		val rSlot: Regex = ".*(<@([\\w\\-]*)/>).*".r

		"package <@pack/> {" match {
			case rSlot("<@pack/>", "pack") =>
				rSlot
		}
	}

	TODO("in the/a lookup; stop looking when classes aren't template-resource subclasses")

	/**
	 * load the/a template's text
	 *
	 * @param name the "name suffix" for the template
	 * @return the single template matching the "name+suffix" pattern. no templates raises an error, and multiple crashes.
	 */
	def load(name: String): Stream[String] =
		classes(getClass)
			.map {
				clazz: Class[_] =>
					// a crude approach; just try to grab any suitable stream
					clazz getResourceAsStream (clazz.getSimpleName.reverse.dropWhile('$' == (_: Char)).reverse + '.' + name)
			}
			.filter(null != _) match {
			case Stream() =>
				error(
					s"cannot find resource `" + (getClass.getSimpleName.reverse.dropWhile('$' == (_: Char)).reverse + '.' + name) + "`"
				)
			case Stream(resourceStream: InputStream) =>
				assume(null != resourceStream, "SAN failed" + s"cannot find resource `???.$name`")
				try {
					val bufferedSource: BufferedSource = Source.fromInputStream(resourceStream)
					try {
						bufferedSource.mkString.splitLines
					} finally {
						bufferedSource.close()
					}
				} finally {
					resourceStream.close()
				}
		}

	def bind[Q](template: String, data: Q)(mine: bind[Q] => bind[Q]): Stream[String] = {
		val rootStackTrace = {
			val rootException = new RuntimeException()
			rootException.getStackTrace.drop(3)
		}

		/**
		 * override the step with a scan, and, append us to that
		 */
		class chain(scan: (String, Q) => Option[Stream[String]]) extends bind[Q] with (String => AnyRef) {

			override def to[T <: Q : ClassTag, O: EnFlat]
			(
				key: String,
				p: T => Boolean
			)(
				value: T => O
			): bind[Q] with (String => AnyRef) = {

				// just our function
				val mine: (String, Q) => Option[Stream[String]] = {
					case (name, q) =>
						if (!(name == key && classFor[T].isInstance(q) && p(classFor[T].cast(q)))) {
							None
						} else {
							Some(
								implicitly[EnFlat[O]].apply(value(classFor[T].cast(q)))
							)
						}
				}

				// aggregated
				new chain(
					(key, q) =>
						scan(key, q)
							.orElse(mine(key, q))
				)
			}

			override def apply(key: String): AnyRef = {

				scan(key, data)
					.getOrElse {
						val runtimeException = new RuntimeException(s"in `$template` couldn't match key=`$key` when data: " + data.getClass.getName)
						runtimeException.setStackTrace(rootStackTrace)
						throw runtimeException
					}
			}
		}
		mine(new chain((_, _) => None)) match {
			case c: chain =>
				bind(template)(c)
		}
	}

	def bind(name: String)(data: String => AnyRef): Stream[String] = {
		val wrap: String => Stream[String] = {
			val cache = new util.HashMap[String, Stream[String]]()
			implicit class PiHashMap[K, V](map: util.HashMap[K, V]) {
				def opt(key: K)(value: => V): V =
					if (map containsKey key)
						map get key
					else {
						map put(key, value)
						map get key
					}
			}

			(key: String) =>
				cache.opt(key) {

					def unravel(value: AnyRef): Stream[String] =
						value match {
							case text: String =>
								Stream(text)

							case list: Iterable[_] =>
								list.toStream.flatMap {
									case ref: AnyRef =>
										unravel(ref)
									case _ =>
										error("this was impossible?")
								}

							case failed =>
								sys.error(
									s"failed to unravel $key `$failed`"
								)
						}

					unravel(data(key))
				}
		}

		def loop(todo: Stream[String]): Stream[String] =
			todo match {
				case (line@rSlot(slot, name)) #:: tail =>
					loop(
						wrap(name).map {
							text: String =>
								line.replace(slot, text)
						} ++ tail
					)
				case head #:: tail =>
					head #:: loop(tail)
				case Stream() =>
					Stream()
			}

		loop {
			load(name)
		}
	}

	/**
	 * scan classes, up the ancestree. we use it to find the/a resource.
	 *
	 * have to scan traits instead the resource is mixed in
	 *
	 */
	private def classes(clazz: Class[_]): Stream[Class[_]] = {
		if (null == clazz || classOf[Object] == clazz)
			Stream()
		else {
			val supe: Class[_] = clazz.getSuperclass
			val tail: Stream[Class[_]] = clazz.getInterfaces.toStream

			val more = (supe #:: tail)

			clazz #:: (more flatMap classes)
		}
	}


	implicit object enFlatString extends EnFlat[String] {
		override def apply(o: String): Stream[String] = Stream(o)
	}

	implicit def enFlatStreamString[S <: Iterable[String]]: EnFlat[S] = new EnFlat[S] {
		override def apply(o: S): Stream[String] = o.toStream
	}

	trait bind[Q] {
		def to[T <: Q : ClassTag, O: EnFlat](key: String, p: T => Boolean)(value: T => O): bind[Q] with (String => AnyRef)

		final def to[T <: Q : ClassTag, O: EnFlat](key: String)(value: T => O): bind[Q] with (String => AnyRef) =
			to(key, (_: T) => true)(value)

		final def to[T <: Q : ClassTag](f: bind[T] => bind[T]): bind[Q] with (String => AnyRef) = {
			error("work this")
		}
	}

	trait EnFlat[O] {
		def apply(o: O): Stream[String]
	}
}
