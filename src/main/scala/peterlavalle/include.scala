package peterlavalle

import java.io.File

//import scala.collection.convert.{DecorateAsJava, DecorateAsScala, ToScalaImplicits}
import scala.reflect.ClassTag

trait include
	extends Error
		with ExtendInputStream
		with PiAutoCloseableT
		with PiDateT
		with PiFileT
		with PiIterableT
		with PiObjectT
		with PiOutputStreamT
		with PiPairT
		with PiPropertiesT
		with PiStringT {

	implicit class PrimpThrowable(e: Throwable) {
		def !(message: String) =
			throw new Exception(message, e)
	}

	@deprecated
	def md5(text: String): String = {
		text.md5
	}

	/**
	 * scan the path environment variable and find the program named as such
	 */
	def onPath(name: String): E[File] = {

		val exts =
			osNameArch {
				case ("windows", _) =>
					List(".cmd", ".bat", ".exe")

				case ("mac", _) =>
					List(".sh", "")

				case ("linux", _) =>
					List(".sh", ".bin", ".elf", "")

				case (what, _) =>
					System.err.println(s"unrecognized OS `$what` so i'll try everything")
					List(".com", ".bat", ".exe", ".sh", ".bin", ".elf", "")
			}

		System.getenv("PATH")
			.split(File.pathSeparatorChar)
			.toStream
			.flatMap {
				path =>
					exts.map(ext => path + File.separator + name + ext)
			}
			.map(new File(_).AbsoluteFile)
			.filter(_.isFile)
			.filter(_.canExecute) match {
			case Stream() =>
				E ! s"Couldn't find executable `$name`"
			case file #:: _ =>
				E(file)
		}
	}

	/**
	 * simplified os/name/arch matching
	 */
	def osNameArch[O: ClassTag](act: (String, String) => O): O =
		act(
			System.getProperty("os.name").takeWhile(' ' != (_: Char)).toLowerCase(),
			System.getProperty("os.arch").takeWhile(' ' != (_: Char)).toLowerCase()
		)

	/**
	 * this dodged some dull compiler issue i can't recall
	 */
	def classFor[T: ClassTag]: Class[T] =
		scala.reflect.classTag[T].runtimeClass.asInstanceOf[Class[T]]

	/**
	 * don't use this - i want to winnow it out
	 */
	@deprecated
	def okayLoop(tm: (String, String))(task: => Unit): Unit = {
		import javax.swing.JOptionPane

		val (title, message) = tm

		@scala.annotation.tailrec
		def loop(): Unit =
			JOptionPane.showConfirmDialog(
				null, message, title, JOptionPane.OK_CANCEL_OPTION
			) match {
				case JOptionPane.OK_OPTION =>
					task
					loop()
				case JOptionPane.CANCEL_OPTION | JOptionPane.CLOSED_OPTION =>
			}

		loop()
	}
}
