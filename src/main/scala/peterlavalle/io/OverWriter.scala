package peterlavalle.io

import peterlavalle._

import java.io._
import java.util

object OverWriter {
	def apply[T](into: File)(act: OverWriter => T): T = {
		new OverWriter(into)
			.using(act)
	}
}

class OverWriter(into: File) extends AutoCloseable {
	val read: ReadSource =
		new ReadSource {
			override def list: Set[String] = data.keySet.toSet

			override def open(name: String): InputStream = new ByteArrayInputStream(data(name))
		}
	private val data = new util.HashMap[String, Array[Byte]]()

	def writer(name: String): Writer =
		new OutputStreamWriter(open(name), "UTF-8")

	def open(name: String): OutputStream =
		new ByteArrayOutputStream() {
			override def close(): Unit = flush()

			override def flush(): Unit = data.put(name, toByteArray)
		}

	def /(path: String) = into / path

	override def close(): Unit = {

		// delete the old files
		scan
			.filterNot(data.keySet.toSet)
			.map(into / _)
			.map(_.delete())
			.foreach(require(_))

		// overwrite the new files
		data.foreach {
			case (name, data) =>
				val file: File = into / name
				// only write if it's missing or the contents have changed
				if (!file.exists() || (file.contents.toList != data.toList))
					file.contents = data
		}
	}

	private def scan: Set[String] = into.**().toSet
}
